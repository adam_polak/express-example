module.exports = {
  apps: [
    {
      name: 'rates-fetcher',
      script: 'src/services/rates/index.js',
    },
    {
      name: 'rates-api',
      script: 'src/services/api/index.js',
      watch: true
    }
  ]
};