const fixtures = require('./fixtures.json');
const { Currency } = require('../models/currency/currency')

const loadFixtures = async connection => {
  await connection.dropDatabase();
  await connection.synchronize();

  const currenciesRepository = connection.getRepository(Currency);

  for (const currency of fixtures.currencies) {
    await  currenciesRepository.save(currency);
  }
};

module.exports = {
  loadFixtures
};