class Rate {
  constructor(id, currencyName, exchangeRate) {
    this.id = id;
    this.currencyName = currencyName;
    this.exchangeRate = exchangeRate;
  }
}

module.exports = {
  Rate
};