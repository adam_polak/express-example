const { EntitySchema } = require('typeorm');
const { Rate } = require('./rate');

module.exports = new EntitySchema({
  name: 'Rate',
  target: Rate,
  columns: {
    id: {
      primary: true,
      type: 'uuid'
    },
    currencyName: {
      type: 'varchar'
    },
    exchangeRate: {
      type: 'decimal'
    }
  },
  relations: {
    currency: {
      type: 'many-to-one',
      target: 'Currency'
    }
  }
});