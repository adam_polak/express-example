const { EntitySchema } = require('typeorm');
const { Currency } = require('./currency');

module.exports = new EntitySchema({
  name: 'Currency',
  target: Currency,
  columns: {
    id: {
      primary: true,
      type: 'uuid'
    },
    currency: {
      type: 'varchar'
    },
    creationDate: {
      type: 'date'
    }
  },
  relations: {
    rates: {
      type: 'one-to-many',
      cascade: true,
      target: 'Rate',
      inverseSide: 'currency'
    }
  }
});