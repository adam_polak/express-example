class Currency {
  constructor(id, currency, rates, creationDate) {
    this.id = id;
    this.currency = currency;
    this.rates = rates;
    this.creationDate = creationDate;
  }
}

module.exports = {
  Currency
};