const exceptionHandler = log => {
  process.on('uncaughtException', err => {
    log(err);
    process.exit(1);
  });

  process.on('unhandledRejection', err => {
    log(err);
    process.exit(1)
  });
};

module.exports = {
  exceptionHandler
};