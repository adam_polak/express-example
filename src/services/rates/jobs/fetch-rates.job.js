const request = require('request-promise');
const { Rate } = require('../../../models/rate/rate');
const { Currency } = require('../../../models/currency/currency');
const { v4 } = require('uuid');

const fetchRates = (repository, config) => async () => {
  try {
    const data = await request({
      method: 'GET',
      url: config.ratesApiEndpoint,
      headers: {
        'currency-api-token': config.ratesApiToken
      },
      json: true
    });

    const rates = data.rates.map(rate => new Rate(
      v4(),
      rate.currency,
      rate.rate
    ));

    const currency = new Currency(
      v4(),
      data.currency,
      rates,
      new Date()
    );

    repository.save(currency);

  } catch(err) {
    console.log(err);
  }
};

module.exports = {
  fetchRates
};