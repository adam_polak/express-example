require('dotenv').config();

const { exceptionHandler } = require('../../shared/exception-handler');
const { scheduleJob } = require('node-schedule');
const { createConnection } = require('typeorm');
const { fetchRates } = require('./jobs/fetch-rates.job');

const { Currency } = require('../../models/currency/currency');


exceptionHandler(console.log);

createConnection()
  .then(connection => {

    const currencyRepository = connection.getRepository(Currency);

    scheduleJob('*/5 * * * * *', fetchRates(currencyRepository, {
      ratesApiEndpoint: process.env.RATES_API_ENDPOINT,
      ratesApiToken: process.env.RATES_API_TOKEN
    }));
  })
  .catch(err => {
    console.log(err)
  });