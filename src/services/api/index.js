require('dotenv').config();

const { exceptionHandler } = require('../../shared/exception-handler');
const { createConnection } = require('typeorm');
const { createApp } = require('./app/app');

exceptionHandler(console.log);

createConnection()
  .then(() => {
    const app = createApp();

    app.listen(3002, () => {
      console.log(`Application is running on: 3002`)
    });
  });
