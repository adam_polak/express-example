const supertest = require('supertest');
const { createApp } = require('../app/app');
const { createConnection } = require('typeorm');
const { loadFixtures } = require('../../../fixtures/load-fixtures');

describe('Server API', () => {
  let connection;

  beforeAll(async () => {
    connection = await createConnection();
    await loadFixtures(connection);
  });

  afterAll(() => {
    return connection.close()
  });

  it('returns 404 on page not found', async () => {
    const app = await createApp();

    return supertest(app)
      .get('/not-found')
      .expect(404);
  });
});