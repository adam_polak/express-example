const express = require('express');
const cors = require('cors');
const { celebrate, Joi, isCelebrate } = require('celebrate');

const { NotFoundError } = require('../errors/not-found.error');
const { createRatesRouting } = require('../rates/rates.routing');

const createServer = (container) => {
  const server = express();

  server.use(cors());
  server.use(express.json());

  server.use('/rates', createRatesRouting(container.currenciesRepository));

  server.post(
    '/example-validation',
    [celebrate({
      body: Joi.object().keys({
        username: Joi.string().required()
      })
    }, {abortEarly: false})],
    (req, res, next) => {
      res.json(body);
    }
  );

  server.use('*', (req, res, next) => next(new NotFoundError('Page not found')));
  server.use((err, req, res, next) => {
    if(isCelebrate(err)) {
      return res.json(err.details);
    }

    if (err instanceof NotFoundError) {
      return res
        .status(err.status)
        .json({
          message: err.message
        })
    }

    next(err);
  });

  return server;
};

module.exports = {
  createServer
};