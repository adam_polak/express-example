const getRates = (repository) => async (req, res, next) => {
  const currencies = await repository.find({
    take: 5,
    skip: 0
  });

  res.json(currencies)
};

module.exports = {
  getRates
};