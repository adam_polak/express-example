const express = require('express');
const { getRates } = require('./rates.controller');

const createRatesRouting = (currenciesRepository) => {
  const router = express.Router();

  router.get('/', getRates(currenciesRepository));

  return router;
};

module.exports = {
  createRatesRouting
};