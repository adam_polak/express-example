const supertest = require('supertest');
const { createApp } = require('../app/app');
const { createConnection } = require('typeorm');
const { loadFixtures } = require('../../../fixtures/load-fixtures');

describe('Rates API', () => {
  let connection;

  beforeAll(async () => {
    connection = await createConnection();
    await loadFixtures(connection);
  });

  afterAll(async () => {
    return connection.close()
  });

  it('returns rates on GET /rates', async () => {
    const app = await createApp();

    return supertest(app)
      .get('/rates')
      .expect(200)
      .then(response => {
        expect(response.body.length).toBe(1);
      });
  });
});