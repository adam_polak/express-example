const { createServer } = require('../server/server');
const { getRepository } = require('typeorm');
const { Currency } = require('../../../models/currency/currency');

const createApp = () => createServer({
  currenciesRepository: getRepository(Currency)
});

module.exports = {
  createApp
};
